/*var express = require('express');
var app = express();*/
var io = require('socket.io')(server);
var server = require('http').Server(app);
var port = 54372;
var express = require('express');
var app = express();
var fs = require('fs');
var https = require('https');
var server = require('http').Server(app);
var io = require('socket.io')(server);
const PORT = 443;
//Import hue api

var hue = require("node-hue-api"),
    HueApi = hue.HueApi,
    lightState = hue.lightState;

var displayResult = function(result) {
    console.log(result);
};

//Define variables for hue apis

var host = "10.0.1.11",
    username = "Jq14cBb32qhcp0ZM49jVLaLbfbI551biugYGU1WP",
    state = lightState.create(),
    api;

api = new HueApi(host, username);


                    /****************
*************************************
                    ARRANQUE INICIAL LAMPARAS
*************************************
                    *****************/

// Set the lamp with id '1' to on
// api.setLightState(1, state.off(), function(err, result)
// {
//     if (err)
//     {
//         throw err;
//         console.log("Error conectando a lampara 1");
//     }
//     displayResult(result);
// })

api.setLightName(1, "Cocina", function(err, result) {
    if (err) throw err;
    displayResults(result);
});

// Set the lamp with id '2' to on
// api.setLightState(2, state.off(), function(err, result)
// {
//     if (err)
//     {
//         throw err;
//         console.log("Error conectando a lampara 1");
//     }
//     displayResult(result);
// });
api.setLightName(2, "Salon", function(err, result) {
    if (err) throw err;
    displayResults(result);
});
// Set the lamp with id '3' to on
// api.setLightState(3, state.off(), function(err, result)
// {
//     if (err)
//     {
//         throw err;
//         console.log("Error conectando a lampara 1");
//     }
//     displayResult(result);
// });
api.setLightName(3, "Pasillo", function(err, result) {
    if (err) throw err;
    displayResults(result);
});









                    /****************
*************************************
                    SOCKET IO
*************************************
                    *****************/

io.on('connection', function(socket) {
    console.log('Conexion establecida desde el cliente.');
    /*
    socket.on('Update', function(data)
    {
        //Manda el estado actual de las bombillas
        console.log("Update " + data.bombilla);
        socket.emit("ack","ack");
    });*/
    socket.on('Encender', function(data)
    {
        //Encender una o todas las bombillas
        console.log("Encender " + data.bombilla);
                api.setLightState(data.bombilla, state.on(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 1");
                    }else{
                        displayResult(result);

                        socket.emit("ack","encender bombilla "+ data.bombilla +" ack");
                    }
                });


    });
    socket.on('Apagar', function(data)
    {
        //Apagar una o todas las bombillas
        console.log("Apagar " + data.bombilla);
                api.setLightState(data.bombilla, state.off(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 1");
                    }else{
                        displayResult(result);

                        socket.emit("ack","apagar bombilla "+ data.bombilla +" ack");
                    }
                });
    });

    socket.on('brilloSubir', function(data)
    {
        //modifica el brillo de una bombilla o todas
        console.log("Subir Brillo " + data.bombilla);
        api.lights(function(err, lights)
        {
            if (err) throw err;
            var light = lights[data.bombilla];
        api.lights(function(err, lights)
        {
            if (err) throw err;
            var indexBombilla = parseInt(data.bombilla)-1
            console.log("Bombilla número: "+data.bombilla)
            var light = lights.lights[indexBombilla];
            displayResult(light);

            console.log("Brillo actual: "+light.state.bri);

            var incremento = 100;
            if(light.state.bri + 100 < 254){
                incremento = ((parseInt(light.state.bri) + 100)/254)*100;
            }
            console.log(incremento);
            api.setLightState(data.bombilla, state.brightness(incremento), function(err, result)
            {
                if (err)
                {
                    throw err;
                    console.log("Error conectando a lampara "+data.bombilla);
                }else{
                    displayResult(result);

                    socket.emit("ack","Bajar Brillo bombilla "+ data.bombilla +" ack");
                }
            });
        });
    });
});

    socket.on('brilloBajar', function(data)
    {
        //modifica el brillo de una bombilla o todas
        console.log("Bajar Brillo " + data.bombilla);

        api.lights(function(err, lights)
        {
            if (err) throw err;
            var indexBombilla = parseInt(data.bombilla)-1
            console.log("Bombilla número: "+data.bombilla)
            var light = lights.lights[indexBombilla];
            displayResult(light);

            console.log("Brillo actual: "+light.state.bri);

            var incremento = 0;
            if(light.state.bri - 100 > 0){
                incremento = ((parseInt(light.state.bri) - 100)/254)*100;
            }
            console.log(incremento)
            api.setLightState(data.bombilla, state.brightness(incremento), function(err, result)
            {
                if (err)
                {
                    throw err;
                    console.log("Error conectando a lampara "+data.bombilla);
                }else{
                    displayResult(result);

                    socket.emit("ack","Bajar Brillo bombilla "+ data.bombilla +" ack");
                }
            });
        });
    });


    socket.on('wifiLevel', function(data)
    {
        //modifica el brillo de todas las bombillas a la vez
        console.log("wifiLevel " + data.level);
        socket.emit("ack","apagar ack");
        var percentage;
        if (data.level >= 7){
                console.log("Caliente");
                percentage = 100;
            api.setLightState(1, state.on().brightness(percentage).rgb(0,255,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(2, state.on().brightness(percentage).rgb(0,255,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla);
                }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(3, state.on().brightness(percentage).rgb(0,255,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });

        }
        else if (data.level > 5){
                console.log("Templado");
                percentage = 66;
                api.setLightState(1, state.brightness(percentage).rgb(0,0,255), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(2, state.brightness(percentage).rgb(0,0,255), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(3, state.brightness(percentage).rgb(0,0,255), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });

        }

        else if (data.level > 2){
                console.log("Frio");
                percentage = 0;
                api.setLightState(1, state.brightness(percentage).rgb(255,140,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(2, state.brightness(percentage).rgb(255,140,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(3, state.brightness(percentage).rgb(255,140,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack");
                }
            });

        }

        else if (data.level > 0){
                console.log("Frio");
                percentage = 0;
                api.setLightState(1, state.brightness(percentage).rgb(255,0,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(2, state.brightness(percentage).rgb(255,0,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });
            api.setLightState(3, state.brightness(percentage).rgb(255,0,0), function(err, result)
            {
                if (err) {throw err; console.log("Error conectando a lampara "+data.bombilla); }else{displayResult(result);  socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack"); }
            });

        }
        console.log("The % is going to be: " + percentage);

    });

/*
255 0 0 -> ROJO
0 255 0 -> VERDE
0 0 255 -> AZUL
255 255 0 -> Amarillo
0 250 255 -> Cyan
75 0 130 -> Morado
*/

socket.on('color', function(data)
{
        console.log("Cambia a "+data.color+" la bombilla "+data.bombilla)
        var r = 255, g = 255, b = 255;
        if (data.color == "rojo"){r = 255, g = 0, b = 0; }
        if (data.color == "verde"){r = 0, g = 255, b = 0; }
        if (data.color == "azul"){r = 0, g = 0, b = 255; }
        if (data.color == "amarillo"){r = 255, g = 255, b = 0; }
        if (data.color == "morado"){r = 90, g = 0, b = 90; }
        api.setLightState(parseInt(data.bombilla) ,state.rgb(r,g,b), function(err, result)
        {
                if (err) {
                    throw err; console.log("Error conectando a lampara "+data.bombilla);
                }else{
                    displayResult(result);

                    socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack");
                }
        });
});

socket.on('valorLux', function(data)
{
        console.log("Modo lux")
        var lux = parseFloat(data.lux);
        var brillo = 100;
        console.log("Lux: "+data.lux)
        if (lux>200){
            brillo = 100;
        }
        else if (lux>175){
            brillo = 85;
        }
        else if (lux>150){
            brillo = 75;
        }
        else if (lux>125){
            brillo = 60;
        }
        else if (lux>75){
            brillo = 40;
        }
        else if (lux>25){
            brillo = 20;
        }
        else if (lux>0){
            brillo = 0;
        }

        api.setLightState(1 ,state.on().brightness(brillo).rgb(255,140,0), function(err, result) {if (err) {throw err; console.log("Error conectando a lampara "+1); }
                else{
                    displayResult(result);
                    console.log("Lampara  "+1+" Brillo a "+brillo);
                }
        });
        api.setLightState(2 ,state.on().brightness(brillo).rgb(255,140,0), function(err, result) {if (err) {throw err; console.log("Error conectando a lampara "+2); }
                else{
                    displayResult(result);
                    console.log("Lampara  "+2+" Brillo a "+brillo);
                }
        });
        api.setLightState(3 ,state.on().brightness(brillo).rgb(255,140,0), function(err, result) {if (err) {throw err; console.log("Error conectando a lampara "+3); }
                else{
                    displayResult(result);
                    console.log("Lampara  "+3+" Brillo a "+brillo);
                }
        });
});


socket.on('Fiesta', function(data)
{
console.log("Recibido comando Fiesta");

var b =0;
var h;


i = 0;
function someAnimation(){
    if (b<3){
        b = b+1;
    }
    else{
        b = 1;
    }
    h = Math.round(Math.random()*65533) + 1;
    console.log("Bombilla: "+b+" Hue: "+h);
    api.setLightState(b, state.transitionInstant().on().hue(h), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });
}
var n = 300;
function animation_loop() {
  someAnimation();
  setTimeout(function() {
    i++;
    if (i < n) {
      animation_loop();
    }
  }, 150);
};
animation_loop();

i = 0;
someAnimation();
setInterval(function() {
  i++;
  if (i < n) {
    someAnimation();
  }
}, 150);
    api.setLightState(1, state.on().transitionInstant().on().rgb(255,0,0).brightness(100), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });
    api.setLightState(2, state.on().transitionInstant().on().rgb(255,0,0).brightness(100), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });
    api.setLightState(3, state.on().transitionInstant().on().rgb(255,0,0).brightness(100), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });

    api.setLightState(1, state.off(), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });
    api.setLightState(2, state.off(), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });
    api.setLightState(3, state.off(), function(err, result)
    {
        if (err) {throw err; console.log("Error conectando a lampara 1"); }
    });


});

   // io.sockets.emit('messages', messages);
});
/*
https.createServer(
{
    key: fs.readFileSync('../cert/jaggy.key'),
    cert: fs.readFileSync('../cert/jaggy.crt')
}, app).listen(PORT, function()
{
    console.log("My https server listening on port " + PORT + "...");
});*/

server.listen(port, function() {
    console.log('Servidor corriendo en http://localhost:' + port);
});



