var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = 54372;

//Import hue api

var hue = require("node-hue-api"),
    HueApi = hue.HueApi,
    lightState = hue.lightState;

var displayResult = function(result) {
    console.log(result);
};

//Define variables for hue apis

var host = "10.0.1.11",
    username = "Jq14cBb32qhcp0ZM49jVLaLbfbI551biugYGU1WP",
    state = lightState.create(),
    api;

api = new HueApi(host, username);


                    /****************
*************************************
                    ARRANQUE INICIAL LAMPARAS
*************************************
                    *****************/
api.setLightState(1, state.off(), function(err, result)
{
    if (err)
    {
        throw err;
        console.log("Error conectando a lampara 1");
    }
    displayResult(result);
});

// Set the lamp with id '1' to on
// api.setLightState(1, state.off(), function(err, result)
// {
//     if (err)
//     {
//         throw err;
//         console.log("Error conectando a lampara 1");
//     }
//     displayResult(result);
// });

api.setLightName(1, "Cocina", function(err, result) {
    if (err) throw err;
    displayResults(result);
});

// Set the lamp with id '2' to on
// api.setLightState(2, state.off(), function(err, result)
// {
//     if (err)
//     {
//         throw err;
//         console.log("Error conectando a lampara 1");
//     }
//     displayResult(result);
// });
api.setLightName(2, "Salon", function(err, result) {
    if (err) throw err;
    displayResults(result);
});
// Set the lamp with id '3' to on
// api.setLightState(3, state.off(), function(err, result)
// {
//     if (err)
//     {
//         throw err;
//         console.log("Error conectando a lampara 1");
//     }
//     displayResult(result);
// });
api.setLightName(3, "Pasillo", function(err, result) {
    if (err) throw err;
    displayResults(result);
});









                    /****************
*************************************
                    SOCKET IO
*************************************
                    *****************/

io.on('connection', function(socket) {
    console.log('Conexion establecida desde el cliente.');

    socket.on('Update', function(data)
    {
        //Manda el estado actual de las bombillas
        console.log("Update " + data.bombilla);
        socket.emit("ack","ack");
    });
    socket.on('Encender', function(data)
    {
        //Encender una o todas las bombillas
        console.log("Encender " + data.bombilla);
        switch(data.bombilla)
        {
            case "1":
                api.setLightState(1, state.on(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 1");
                    }else{
                        displayResult(result);
                        console.log("conectando a lampara 1");
                        socket.emit("ack","encender bombilla "+ data.bombilla +" ack");
                    }
                });
                break;
            case "2":
                api.setLightState(2, state.on(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 2");
                    }else{
                        displayResult(result);
                        console.log("conectando a lampara 2");
                        socket.emit("ack","encender bombilla "+ data.bombilla +" ack");
                    }
                });
                break;
            case "3":
                api.setLightState(3, state.on(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 3");
                    }else{
                        displayResult(result);
                        console.log("conectando a lampara 3");
                        socket.emit("ack","encender bombilla "+ data.bombilla +" ack");
                    }
                });
                break;
            default:
                    console.log("intentando conectar a lampara "+data.bombilla + " pero no existe");
                        socket.emit("ack","encender bombilla "+ data.bombilla +" ack");
                break;
        }

    });
    socket.on('Apagar', function(data)
    {
        //Apagar una o todas las bombillas
        console.log("Apagar " + data.bombilla);
        switch(data.bombilla)
        {
            case "1":
                api.setLightState(1, state.off(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 1");
                    }else{
                        displayResult(result);
                        console.log("conectando a lampara 1");
                        socket.emit("ack","apagar bombilla "+ data.bombilla +" ack");
                    }
                });
                break;
            case "2":
                api.setLightState(2, state.off(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 2");
                    }else{
                        displayResult(result);
                        console.log("conectando a lampara 2");
                        socket.emit("ack","apagar bombilla "+ data.bombilla +" ack");
                    }
                });
                break;
            case "3":
                api.setLightState(3, state.off(), function(err, result)
                {
                    if (err)
                    {
                        throw err;
                        console.log("Error conectando a lampara 3");
                    }else{
                        displayResult(result);
                        console.log("conectando a lampara 3");
                        socket.emit("ack","apagar bombilla "+ data.bombilla +" ack");
                    }
                });
                break;
            default:
                    console.log("intentando apagar a lampara "+data.bombilla + " pero no existe");
                        socket.emit("ack","apagar bombilla "+ data.bombilla +" ack");
                break;
        }
    });

    socket.on('brilloSubir', function(data)
    {
        //modifica el brillo de una bombilla o todas
        console.log("Subir Brillo " + data.bombilla);
        api.lights(function(err, lights)
        {
            if (err) throw err;
            var light = lights[data.bombilla];
        api.lights(function(err, lights)
        {
            if (err) throw err;
            var indexBombilla = parseInt(data.bombilla)-1
            console.log("Bombilla número: "+data.bombilla)
            var light = lights.lights[indexBombilla];
            displayResult(light);

            console.log("Brillo actual: "+light.state.bri);

            var incremento = 100;
            if(light.state.bri + 100 < 254){
                console.log("No paso por aqui....");
                incremento = ((parseInt(light.state.bri) + 100)/254)*100;
            }
            console.log(incremento);
            api.setLightState(data.bombilla, state.brightness(incremento), function(err, result)
            {
                if (err)
                {
                    throw err;
                    console.log("Error conectando a lampara "+data.bombilla);
                }else{
                    displayResult(result);
                    console.log("conectando a lampara "+data.bombilla);
                    socket.emit("ack","Bajar Brillo bombilla "+ data.bombilla +" ack");
                }
            });
            });
    });
});

    socket.on('brilloBajar', function(data)
    {
        //modifica el brillo de una bombilla o todas
        console.log("Bajar Brillo " + data.bombilla);

        api.lights(function(err, lights)
        {
            if (err) throw err;
            var indexBombilla = parseInt(data.bombilla)-1
            console.log("Bombilla número: "+data.bombilla)
            var light = lights.lights[indexBombilla];
            displayResult(light);

            console.log("Brillo actual: "+light.state.bri);

            var incremento = 0;
            if(light.state.bri - 100 > 0){
                incremento = ((parseInt(light.state.bri) - 100)/254)*100;
            }
            console.log(incremento)
            api.setLightState(data.bombilla, state.brightness(incremento), function(err, result)
            {
                if (err)
                {
                    throw err;
                    console.log("Error conectando a lampara "+data.bombilla);
                }else{
                    displayResult(result);
                    console.log("conectando a lampara "+data.bombilla);
                    socket.emit("ack","Bajar Brillo bombilla "+ data.bombilla +" ack");
                }
            });
            });
    });


    socket.on('wifiLevel', function(data)
    {
        //modifica el brillo de todas las bombillas a la vez
        console.log("wifiLevel " + data.level);
        socket.emit("ack","apagar ack");
        var percentage = data.level/2;
        console.log("The % is going to be: " + percentage);

            api.setLightState(data.bombilla, state.brightness(percentage), function(err, result)
            {
                if (err)
                {
                    throw err;
                    console.log("Error conectando a lampara "+data.bombilla);
                }else{
                    displayResult(result);
                    console.log("conectando a lampara "+data.bombilla);
                    socket.emit("ack","Seguimiento bombilla "+ data.bombilla +" ack");
                }
            });

    });


   // io.sockets.emit('messages', messages);
});


server.listen(port, function() {
    console.log('Servidor corriendo en http://localhost:' + port);
});



var express = require('express');
var app = express();
var fs = require('fs');
var https = require('https');
var server = require('http').Server(app);
var io = require('socket.io')(server);
CONST PORT = 443;



https.createServer(
{
    key: fs.readFileSync('cert/jaggy.key'),
    cert: fs.readFileSync('cert/jaggy.crt')   
}, app).listen(PORT, function()
{
    console.log("My https server listening on port " + PORT + "...");
});